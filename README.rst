.. image:: https://travis-ci.org/mcepl/bayeux.svg?branch=master
    :target: https://travis-ci.org/mcepl/bayeux

Bayeux provides a module for generating TAP stream
(http://testanything.org/). Unfortunately, the name tapestry has been
already taken, so I took an inspiration from
https://en.wikipedia.org/wiki/Bayeux_Tapestry .

Aside from the script for generating the TAP stream programtically,
there is also a version of unittest2 which generates TAP stream instead of
the standard output of unittest.

Whole this package is licensed under MIT/X11 license (see header of tap.py
for details). 
